﻿using Mateusz_Trojan_Project.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace Mateusz_Trojan_Project.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Fields
        private ReadOnlyCollection<CommandViewModel> _Commands;
        private ObservableCollection<WorkspaceViewModel> _Workspaces;
        #endregion

        #region MenuCommands
        private BaseCommand _CreatePlaylistCommand;
        public ICommand CreatePlaylistCommand
        {
            get
            {
                if (_CreatePlaylistCommand == null)
                    _CreatePlaylistCommand = new BaseCommand(() => CreatePlaylist());
                return _CreatePlaylistCommand;
            }
        }
        private BaseCommand _FirstListCommand;
        public ICommand FirstListCommand
        {
            get
            {
                if (_FirstListCommand == null)
                    _FirstListCommand = new BaseCommand(() => FirstList());
                return _FirstListCommand;
            }
        }
        private BaseCommand _SecondListCommand;
        public ICommand SecondListCommand
        {
            get
            {
                if (_SecondListCommand == null)
                    _SecondListCommand = new BaseCommand(() => SecondList());
                return _SecondListCommand;
            }
        }
        private BaseCommand _ThirdListCommand;
        public ICommand ThirdListCommand
        {
            get
            {
                if (_ThirdListCommand == null)
                    _ThirdListCommand = new BaseCommand(() => ThirdList());
                return _ThirdListCommand;
            }
        }
        private BaseCommand _FourthListCommand;
        public ICommand FourthListCommand
        {
            get
            {
                if (_FourthListCommand == null)
                    _FourthListCommand = new BaseCommand(() => FourthList());
                return _FourthListCommand;
            }
        }
        private BaseCommand _FifthListCommand;
        public ICommand FifthListCommand
        {
            get
            {
                if (_FifthListCommand == null)
                    _FifthListCommand = new BaseCommand(() => FifthList());
                return _FifthListCommand;
            }
        }
        private BaseCommand _UserAccountCommand;
        public ICommand UserAccountCommand
        {
            get
            {
                if (_UserAccountCommand == null)
                    _UserAccountCommand = new BaseCommand(() => UserAccount());
                return _UserAccountCommand;
            }
        }
        #endregion

        #region Commands
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_Commands == null)
                {
                    List<CommandViewModel> cmds = this.CreateCommands();
                    _Commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }
                return _Commands;
            }
        }
        private List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>
            {
                new CommandViewModel("Utwórz playlistę", new BaseCommand(() => this.CreatePlaylist())),
                new CommandViewModel("Lista nr 1", new BaseCommand(()=> this.FirstList())),
                new CommandViewModel("Lista nr 2", new BaseCommand(()=> this.SecondList())),
                new CommandViewModel("Lista nr 3", new BaseCommand(()=> this.ThirdList())),
                new CommandViewModel("Lista nr 4", new BaseCommand(()=> this.FourthList())),
                new CommandViewModel("Lista nr 5", new BaseCommand(()=> this.FifthList())),
                new CommandViewModel("Twoje konto", new BaseCommand(()=> this.UserAccount()))
            };
        }
        #endregion

        #region Workspaces
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (_Workspaces == null)
                {
                    _Workspaces = new ObservableCollection<WorkspaceViewModel>();
                    _Workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return _Workspaces;
            }
        }
        private void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;
            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }
        private void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            this.Workspaces.Remove(workspace);
        }
        #endregion

        #region PrivateHelpers
        private void UserAccount()
        {
            UserAccountViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is UserAccountViewModel) as UserAccountViewModel;
            if (workspace == null)
            {
                workspace = new UserAccountViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void FirstList()
        {
            FirstListViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is FirstListViewModel) as FirstListViewModel;
            if (workspace == null)
            {
                workspace = new FirstListViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void SecondList()
        {
            SecondListViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is SecondListViewModel) as SecondListViewModel;
            if (workspace == null)
            {
                workspace = new SecondListViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void ThirdList()
        {
            ThirdListViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is ThirdListViewModel) as ThirdListViewModel;
            if (workspace == null)
            {
                workspace = new ThirdListViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void FourthList()
        {
            FourthListViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is FourthListViewModel) as FourthListViewModel;
            if (workspace == null)
            {
                workspace = new FourthListViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void FifthList()
        {
            FifthListViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is FifthListViewModel) as FifthListViewModel;
            if (workspace == null)
            {
                workspace = new FifthListViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void CreatePlaylist()
        {
            CreatePlaylistViewModel workspace = this.Workspaces.FirstOrDefault(vm => vm is CreatePlaylistViewModel) as CreatePlaylistViewModel;
            if (workspace == null)
            {
                workspace = new CreatePlaylistViewModel();
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }
        private void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            Debug.Assert(this.Workspaces.Contains(workspace));
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }
        #endregion
    }
}